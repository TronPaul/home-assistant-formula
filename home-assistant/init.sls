{%- from "home-assistant/map.jinja" import home_assistant with context %}

deps:
  pkg.installed:
    - names: {{ home_assistant.deps }}

hass:
  user.present:
    - system: true
    - gid: nogroup
    - groups:
      - dialout
    - home: /var/lib/hass

hass-venv:
  virtualenv.manage:
    - name: {{ home_assistant.venv }}
    - user: hass
    - system_site_packages: True
    - pip_pkgs:
      - homeassistant
      - cython==0.24.1
    - python: /usr/bin/python3
    - require:
      - pkg: deps
      - file: home-assistant-dirs

home-assistant-dirs:
  file.directory:
    - names:
      - /var/lib/hass
      - {{ home_assistant.config_path }}
      - /var/log/hass
    - mode: 700
    - makedirs: true
    - user: hass
    - group: nogroup

install-zwave:
  file.directory:
    - name: /usr/src/python-openzwave
    - user: hass
  git.latest:
    - name: https://github.com/OpenZWave/python-openzwave.git
    - branch: python3
    - target: /usr/src/python-openzwave
    - user: hass
    - depth: 1
    - require:
      - file: install-zwave
  cmd.run:
    - name: make build && make install
    - cwd: /usr/src/python-openzwave
    - runas: hass
    - unless: {{ home_assistant.venv }}/bin/pip3 list 2>/dev/null | grep -q openzwave
    - env:
      - PYTHON_EXEC: {{ home_assistant.venv }}/bin/python3
    - require:
      - git: install-zwave
      - virtualenv: hass-venv

{{ home_assistant.config_path }}/configuration.yaml:
{% if salt['pillar.get']('home-assistant:lookup:config') is not none %}
  file.serialize:
    - dataset_pillar: home-assistant:lookup:config
    - formatter: YAML
    - user: hass
    - group: nogroup
    - watch_in:
      - service: home-assistant-service
{% else %}
  file.managed:
    - source: salt://home-assistant/configuration.yaml.jinja
    - template: jinja
    - context:
        db_url: {{ home_assistant.db_url }}
    - user: hass
    - group: nogroup
    - watch_in:
      - service: home-assistant-service
{% endif %}

{% if salt['pillar.get']('home-assistant:lookup:secrets') is not none %}
{{ home_assistant.config_path }}/secrets.yaml:
  file.serialize:
    - dataset_pillar: home-assistant:lookup:secrets
    - formatter: YAML
    - user: hass
    - group: nogroup
    - watch_in:
      - service: home-assistant-service
{% endif %}

home-assistant-service:
  file.managed:
    - name: /etc/systemd/system/home-assistant@hass.service
    - source: salt://home-assistant/home-assistant@.service.jinja
    - template: jinja
    - context:
        venv: {{ home_assistant.venv }}
        config_path: {{ home_assistant.config_path }}
  service.running:
    - name: home-assistant@hass
    - enable: true
    - require:
      - file: home-assistant-service
